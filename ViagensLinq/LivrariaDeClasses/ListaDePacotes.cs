﻿namespace LivrariaDeClasses
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class ListaDePacotes
    {

        private static List<Pacote> listaDePacotes = new List<Pacote>();
        private static string localizarFicheiro = null;

        public static List<Pacote> LoadPacotes()
        {
            List<Pacote> output = new List<Pacote>();
            {
                output.Add(new Pacote { IdPacote = 1, Descricao = "Republica Dominicana", Preco = 900.00 });
                output.Add(new Pacote { IdPacote = 2, Descricao = "Seyschelless", Preco = 1200.00 });
                output.Add(new Pacote { IdPacote = 3, Descricao = "México Imperial", Preco = 980.00 });
                output.Add(new Pacote { IdPacote = 4, Descricao = "Egipto Faraónico", Preco = 750.00 });
                output.Add(new Pacote { IdPacote = 5, Descricao = "Tunisia", Preco = 600.00 });
            }
            return output;
        }
        public static void CriarPacote(Pacote pacote)
        {
         
            listaDePacotes.Add(pacote);
        }
        public static int GeraPacote(List<Pacote> listaDePacotes)


        {
            if (listaDePacotes.Count != 0)
            {
                return listaDePacotes[listaDePacotes.Count - 1].IdPacote + 1;
            }
            else
            {
                return 1;
            }


        }
        public static void Gravar(List<Pacote> listaDePacotes)
        {
            if (localizarFicheiro != null)
            {
                StreamWriter sw = new StreamWriter(localizarFicheiro);
                try
                {
                    if (!File.Exists(localizarFicheiro))
                    {
                        sw = File.CreateText(localizarFicheiro);
                    }

                    foreach (var pacote in listaDePacotes)
                    {
                        string linha = string.Format("{0};{1};{2}", pacote.IdPacote, pacote.Descricao, pacote.Preco);
                        sw.WriteLine(linha);
                    }
                    MessageBox.Show("Ficheiro guardado com sucesso!");
                    sw.Close();
                }
                catch (Exception ev)
                {
                    MessageBox.Show(ev.Message);
                }
            }
            else
            {
                MessageBox.Show("A lista está vazia. Abra o ficheiro para carregar...");
            }

        }
    }
