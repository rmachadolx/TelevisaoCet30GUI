﻿


namespace ViagensLinq
{
    using System.Windows.Forms;
    using System.Collections.Generic;
    using LivrariaDeClasses;

    public partial class FormApagarPacotes : Form
    {
        private List<Pacote> ListaDePacotes = new List<Pacote>();


        Pacote pacote = new Pacote();
        public FormApagarPacotes()
        {
            InitializeComponent();
           // this.ListaDePacotes = ListaDePacotes;
            ComboBoxListarPacotes.DataSource = ListaDePacotes;
        }

     

     

        private void ButtonApagarPacote_Click(object sender, System.EventArgs e)
        {
            int indice = 0;

            indice = ComboBoxListarPacotes.SelectedIndex;

            if (indice == -1)
            {
                MessageBox.Show("Não escolheu um pacote para apagar.");
                return;
            }

            ListaDePacotes.RemoveAt(indice);

            ComboBoxListarPacotes.DataSource = null;
            ComboBoxListarPacotes.DataSource = ListaDePacotes;
      
            MessageBox.Show("Pacote apagado com sucesso.");
        }
        private void ButtunSair_Click(object sender, System.EventArgs e)
        {

            this.Close();

        }
    }
}
