﻿
namespace ViagensLinq

{
    using System;
    using LivrariaDeClasses;

    using System.Collections.Generic;
    using System.Windows.Forms;


    public partial class FormListarPacotes : Form
    {
        List<Pacote> listaDePacotes = ListaDePacotes.LoadPacotes();
       // Pacote pacote = new Pacote();
        public FormListarPacotes()
        {

            InitializeComponent();
     

            DataGridViewPacotes.DataSource = listaDePacotes;
            DataGridViewPacotes.AllowUserToAddRows = false;
            DataGridViewPacotes.Columns[0].HeaderText = "ID Pacote";
            DataGridViewPacotes.Columns[1].HeaderText = "Descrição";
            DataGridViewPacotes.Columns[2].HeaderText = "Preço";

        }

      
        private void ButtonSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
