﻿
namespace ViagensLinq
{
    using System.Collections.Generic;
    using System.Windows.Forms;

    using System;
    using LivrariaDeClasses;

    public partial class FormEditarPacotes : Form
    {
        List<Pacote> listaDePacotes = ListaDePacotes.LoadPacotes();
        int indice = 0;

        Pacote pacote = new Pacote();

        public FormEditarPacotes()
        {

            InitializeComponent();
        
            if (listaDePacotes.Count != 0)
            {
                ActualizaDados(indice);
            }

        }


        

     
        private void ButtonDiminuirIdPacote_Click(object sender, EventArgs e)
        {
            if (listaDePacotes.Count != 0)
            {

                indice--;
                if (indice < 0)
                {
                    indice = listaDePacotes.Count - 1;
                }
                ActualizaDados(indice);
            }

        }

        private void ButtonFimIdPacote_Click(object sender, EventArgs e)
        {

            if (listaDePacotes.Count != 0)
            {
                indice = listaDePacotes.Count - 1;
                ActualizaDados(indice);
            }
        }

        private void ButtonInicioIdPacote_Click(object sender, EventArgs e)
        {
            if (listaDePacotes.Count != 0)
            {
                indice = 0;
                ActualizaDados(indice);
            }
        }


        private void ActualizaDados(int indice)
        {
            TextBoxIdPacote.Text = listaDePacotes[indice].IdPacote.ToString();
            TextBoxDescricao.Text = listaDePacotes[indice].Descricao;
            TextBoxPreco.Text = listaDePacotes[indice].Preco.ToString();
        }

        private void ButtonSalvarPacote_Click(object sender, EventArgs e)
        {
            if (listaDePacotes[indice].Descricao == TextBoxDescricao.Text && listaDePacotes[indice].Preco == Convert.ToDouble(TextBoxPreco.Text))
            {
                MessageBox.Show("Atenção! Não fez nenhuma alteração.");
            }
            else
            {
                listaDePacotes[indice].Descricao = TextBoxDescricao.Text;
                listaDePacotes[indice].Preco = Convert.ToDouble(TextBoxPreco.Text);
                MessageBox.Show("Pacote alterado com Sucesso!");

            }

        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonAumentarIdPacote_Click(object sender, EventArgs e)
        {
            if (listaDePacotes.Count != 0)
            {

                indice++;
                if (indice > listaDePacotes.Count - 1)
                {
                    indice = 0;
                }
                ActualizaDados(indice);
            }

        }

     
    }
}
