﻿
namespace ViagensLinq
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using ViagensLinq;
    using System.IO;
    using LivrariaDeClasses;

    public partial class Form1 : Form
    {
        List<Pacote> listaDePacotes = ListaDePacotes.LoadPacotes();

        private FormNovoPacote FormulariroNovoPacote;

        private FormListarPacotes FormularioListarPacotes;
        private FormEditarPacotes FormularioEditarPacotes;
        private FormApagarPacotes FormularioApagarPacotes;
     
        public Form1()
        {
            InitializeComponent();

        }

        private void ButtonCreate_Click(object sender, EventArgs e)
        {
            FormulariroNovoPacote = new FormNovoPacote();
            FormulariroNovoPacote.Show();

        }

        private void ButtunRead_Click(object sender, EventArgs e)
        {
            FormularioListarPacotes = new FormListarPacotes();
            FormularioListarPacotes.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pacote de Viagens 1.0.0\npor Rui Machado\nData:05/10/2017");
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            FormularioEditarPacotes = new FormEditarPacotes();
            FormularioEditarPacotes.Show();
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            FormularioApagarPacotes = new FormApagarPacotes();
            FormularioApagarPacotes.Show();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StreamReader myStream;
            OpenFileDialog ficheiro = new OpenFileDialog();

            //ficheiro.InitialDirectory = "c:\\";
            ficheiro.Filter = "Text file|*.txt";
            ficheiro.Title = "Save an text File";
            //ficheiro.FilterIndex = 2;
            ficheiro.RestoreDirectory = true;

            if (ficheiro.ShowDialog() == DialogResult.OK)
            {
      

            }
        }

        private void salvarToolStripMenuItem_Click(object sender, EventArgs e)

        {
            //ListaDePacotes.Gravar();
        }

        private void salvarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog ficheiro = new SaveFileDialog();

            //ficheiro.InitialDirectory = "c:\\";
            ficheiro.Filter = "Text file|*.txt";
            ficheiro.Title = "Save an text File";
            ficheiro.ShowDialog();

            if (ficheiro.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                FileStream fs = (FileStream)ficheiro.OpenFile();

                StreamWriter sw = new StreamWriter(fs);//se nao existir, grava

                //verficar se existe algum problema


                foreach (var pacote in listaDePacotes)
                {
                    string linha = string.Format("{0};{1};{2}", pacote.IdPacote, pacote.Descricao, pacote.Preco);
                    sw.WriteLine(linha);

                }
                sw.Close();


                fs.Close();
            }

        }

    }
}





