﻿namespace ViagensLinq
{
    partial class FormApagarPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormApagarPacotes));
            this.ComboBoxListarPacotes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtunSair = new System.Windows.Forms.Button();
            this.ButtonApagarPacote = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComboBoxListarPacotes
            // 
            this.ComboBoxListarPacotes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListarPacotes.FormattingEnabled = true;
            this.ComboBoxListarPacotes.Location = new System.Drawing.Point(161, 35);
            this.ComboBoxListarPacotes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ComboBoxListarPacotes.Name = "ComboBoxListarPacotes";
            this.ComboBoxListarPacotes.Size = new System.Drawing.Size(556, 24);
            this.ComboBoxListarPacotes.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "Lista de Pacotes:";
            // 
            // ButtunSair
            // 
            this.ButtunSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunSair.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.ButtunSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtunSair.Image")));
            this.ButtunSair.Location = new System.Drawing.Point(648, 97);
            this.ButtunSair.Margin = new System.Windows.Forms.Padding(4);
            this.ButtunSair.Name = "ButtunSair";
            this.ButtunSair.Size = new System.Drawing.Size(69, 66);
            this.ButtunSair.TabIndex = 17;
            this.ButtunSair.UseVisualStyleBackColor = false;
            this.ButtunSair.Click += new System.EventHandler(this.ButtunSair_Click);
            // 
            // ButtonApagarPacote
            // 
            this.ButtonApagarPacote.Image = ((System.Drawing.Image)(resources.GetObject("ButtonApagarPacote.Image")));
            this.ButtonApagarPacote.Location = new System.Drawing.Point(161, 97);
            this.ButtonApagarPacote.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonApagarPacote.Name = "ButtonApagarPacote";
            this.ButtonApagarPacote.Size = new System.Drawing.Size(69, 66);
            this.ButtonApagarPacote.TabIndex = 19;
            this.ButtonApagarPacote.UseVisualStyleBackColor = true;
            this.ButtonApagarPacote.Click += new System.EventHandler(this.ButtonApagarPacote_Click);
            // 
            // FormApagarPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 198);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonApagarPacote);
            this.Controls.Add(this.ButtunSair);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxListarPacotes);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormApagarPacotes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Apagar Pacotes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxListarPacotes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtunSair;
        private System.Windows.Forms.Button ButtonApagarPacote;
    }
}