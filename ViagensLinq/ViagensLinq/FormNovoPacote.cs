﻿
namespace ViagensLinq
{
    using System;
    using System.Windows.Forms;

    using System.Collections.Generic;
    using LivrariaDeClasses;

    public partial class FormNovoPacote : Form


    {
    
        List<Pacote> Pacotes = ListaDePacotes.LoadPacotes();

        Pacote pacote = new Pacote();

        public FormNovoPacote()
        {
            InitializeComponent();
     
            TextBoxIdPacote.Text = ListaDePacotes.GeraPacote(Pacotes).ToString();

        }

        private void ButtunSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonNovoPacote_Click(object sender, EventArgs e)
        {
            double Price;

            if (string.IsNullOrEmpty(TextBoxDescricao.Text))
            {
                MessageBox.Show("Tem que inserir a descrição do pagote.");
                return;
            }

            if (!double.TryParse(TextBoxPreco.Text, out (Price)))
            {
                MessageBox.Show("Tem que inserir o preço dom pacote.");
                return;
            }
            var pacote = new Pacote
            {
               // IdPacote = GeraPacote(),
                Descricao = TextBoxDescricao.Text,
                Preco = Convert.ToDouble(TextBoxPreco.Text)
            };

            ListaDePacotes.CriarPacote(pacote);
         
            MessageBox.Show("Novo pacote inserido com sucesso");
            Close();
        }

    
    }
}
