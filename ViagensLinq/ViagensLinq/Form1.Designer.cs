﻿namespace ViagensLinq
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ButtunSair = new System.Windows.Forms.Button();
            this.ButtunDelete = new System.Windows.Forms.Button();
            this.ButtunUpdate = new System.Windows.Forms.Button();
            this.ButtunRead = new System.Windows.Forms.Button();
            this.ButtunCreate = new System.Windows.Forms.Button();
            this.ToolTipListar = new System.Windows.Forms.ToolTip(this.components);
            this.ToolTipEditar = new System.Windows.Forms.ToolTip(this.components);
            this.ToolTipApagar = new System.Windows.Forms.ToolTip(this.components);
            this.ToolTipNovo = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ficheiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtunSair
            // 
            this.ButtunSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtunSair.Image")));
            this.ButtunSair.Location = new System.Drawing.Point(307, 230);
            this.ButtunSair.Name = "ButtunSair";
            this.ButtunSair.Size = new System.Drawing.Size(174, 89);
            this.ButtunSair.TabIndex = 4;
            this.ButtunSair.UseVisualStyleBackColor = false;
            this.ButtunSair.Click += new System.EventHandler(this.button1_Click);
            // 
            // ButtunDelete
            // 
            this.ButtunDelete.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ButtunDelete.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunDelete.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ButtunDelete.Image = ((System.Drawing.Image)(resources.GetObject("ButtunDelete.Image")));
            this.ButtunDelete.Location = new System.Drawing.Point(397, 135);
            this.ButtunDelete.Name = "ButtunDelete";
            this.ButtunDelete.Size = new System.Drawing.Size(84, 89);
            this.ButtunDelete.TabIndex = 3;
            this.ToolTipApagar.SetToolTip(this.ButtunDelete, "Apagar Pacotes de viagem");
            this.ButtunDelete.UseVisualStyleBackColor = false;
            this.ButtunDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtunUpdate
            // 
            this.ButtunUpdate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunUpdate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunUpdate.Image = ((System.Drawing.Image)(resources.GetObject("ButtunUpdate.Image")));
            this.ButtunUpdate.Location = new System.Drawing.Point(307, 135);
            this.ButtunUpdate.Name = "ButtunUpdate";
            this.ButtunUpdate.Size = new System.Drawing.Size(84, 89);
            this.ButtunUpdate.TabIndex = 2;
            this.ToolTipListar.SetToolTip(this.ButtunUpdate, "Editar Pacotes de viagem");
            this.ButtunUpdate.UseVisualStyleBackColor = false;
            this.ButtunUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // ButtunRead
            // 
            this.ButtunRead.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunRead.Image = ((System.Drawing.Image)(resources.GetObject("ButtunRead.Image")));
            this.ButtunRead.Location = new System.Drawing.Point(397, 40);
            this.ButtunRead.Name = "ButtunRead";
            this.ButtunRead.Size = new System.Drawing.Size(84, 89);
            this.ButtunRead.TabIndex = 1;
            this.ToolTipListar.SetToolTip(this.ButtunRead, "Listar Pacotes");
            this.ButtunRead.UseVisualStyleBackColor = false;
            this.ButtunRead.Click += new System.EventHandler(this.ButtunRead_Click);
            // 
            // ButtunCreate
            // 
            this.ButtunCreate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunCreate.Image = ((System.Drawing.Image)(resources.GetObject("ButtunCreate.Image")));
            this.ButtunCreate.Location = new System.Drawing.Point(307, 40);
            this.ButtunCreate.Name = "ButtunCreate";
            this.ButtunCreate.Size = new System.Drawing.Size(84, 89);
            this.ButtunCreate.TabIndex = 0;
            this.ButtunCreate.Text = " ";
            this.ToolTipNovo.SetToolTip(this.ButtunCreate, "Novo Pacote de Viagens");
            this.ButtunCreate.UseVisualStyleBackColor = false;
            this.ButtunCreate.Click += new System.EventHandler(this.ButtonCreate_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ficheiroToolStripMenuItem,
            this.sobreToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1213, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ficheiroToolStripMenuItem
            // 
            this.ficheiroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem,
            this.salvarToolStripMenuItem,
            this.salvarComoToolStripMenuItem});
            this.ficheiroToolStripMenuItem.Name = "ficheiroToolStripMenuItem";
            this.ficheiroToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.ficheiroToolStripMenuItem.Text = "Ficheiro";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // salvarToolStripMenuItem
            // 
            this.salvarToolStripMenuItem.Name = "salvarToolStripMenuItem";
            this.salvarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salvarToolStripMenuItem.Text = "Salvar";
            this.salvarToolStripMenuItem.Click += new System.EventHandler(this.salvarToolStripMenuItem_Click);
            // 
            // salvarComoToolStripMenuItem
            // 
            this.salvarComoToolStripMenuItem.Name = "salvarComoToolStripMenuItem";
            this.salvarComoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salvarComoToolStripMenuItem.Text = "Salvar como...";
            this.salvarComoToolStripMenuItem.Click += new System.EventHandler(this.salvarComoToolStripMenuItem_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.sobreToolStripMenuItem.Text = "Sobre...";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1213, 633);
            this.ControlBox = false;
            this.Controls.Add(this.ButtunSair);
            this.Controls.Add(this.ButtunDelete);
            this.Controls.Add(this.ButtunUpdate);
            this.Controls.Add(this.ButtunRead);
            this.Controls.Add(this.ButtunCreate);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Viagens";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtunCreate;
        private System.Windows.Forms.Button ButtunRead;
        private System.Windows.Forms.Button ButtunUpdate;
        private System.Windows.Forms.Button ButtunDelete;
        private System.Windows.Forms.Button ButtunSair;


        private System.Windows.Forms.ToolTip ToolTipApagar;
        private System.Windows.Forms.ToolTip ToolTipListar;
        private System.Windows.Forms.ToolTip ToolTipEditar;
        private System.Windows.Forms.ToolTip ToolTipNovo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ficheiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvarComoToolStripMenuItem;
    }
}

