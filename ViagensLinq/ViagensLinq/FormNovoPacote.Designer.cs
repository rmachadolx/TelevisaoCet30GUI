﻿namespace ViagensLinq
{
    partial class FormNovoPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNovoPacote));
            this.TextBoxPreco = new System.Windows.Forms.TextBox();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.TextBoxIdPacote = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtunSair = new System.Windows.Forms.Button();
            this.ButtonNovoPacote = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxPreco
            // 
            this.TextBoxPreco.Location = new System.Drawing.Point(258, 196);
            this.TextBoxPreco.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxPreco.Name = "TextBoxPreco";
            this.TextBoxPreco.Size = new System.Drawing.Size(232, 22);
            this.TextBoxPreco.TabIndex = 12;
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Location = new System.Drawing.Point(258, 97);
            this.TextBoxDescricao.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.Size = new System.Drawing.Size(638, 22);
            this.TextBoxDescricao.TabIndex = 11;
            // 
            // TextBoxIdPacote
            // 
            this.TextBoxIdPacote.Location = new System.Drawing.Point(258, 25);
            this.TextBoxIdPacote.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxIdPacote.Name = "TextBoxIdPacote";
            this.TextBoxIdPacote.ReadOnly = true;
            this.TextBoxIdPacote.Size = new System.Drawing.Size(232, 22);
            this.TextBoxIdPacote.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(58, 196);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Preço ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(58, 102);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Descrição :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(58, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "ID Pacote :";
            // 
            // ButtunSair
            // 
            this.ButtunSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtunSair.ForeColor = System.Drawing.SystemColors.Highlight;
            this.ButtunSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtunSair.Image")));
            this.ButtunSair.Location = new System.Drawing.Point(776, 327);
            this.ButtunSair.Margin = new System.Windows.Forms.Padding(4);
            this.ButtunSair.Name = "ButtunSair";
            this.ButtunSair.Size = new System.Drawing.Size(123, 90);
            this.ButtunSair.TabIndex = 1;
            this.ButtunSair.UseVisualStyleBackColor = false;
            this.ButtunSair.Click += new System.EventHandler(this.ButtunSair_Click);
            // 
            // ButtonNovoPacote
            // 
            this.ButtonNovoPacote.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtonNovoPacote.Image = ((System.Drawing.Image)(resources.GetObject("ButtonNovoPacote.Image")));
            this.ButtonNovoPacote.Location = new System.Drawing.Point(258, 327);
            this.ButtonNovoPacote.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonNovoPacote.Name = "ButtonNovoPacote";
            this.ButtonNovoPacote.Size = new System.Drawing.Size(123, 90);
            this.ButtonNovoPacote.TabIndex = 13;
            this.ButtonNovoPacote.UseVisualStyleBackColor = false;
            this.ButtonNovoPacote.Click += new System.EventHandler(this.ButtonNovoPacote_Click);
            // 
            // FormNovoPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(960, 458);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonNovoPacote);
            this.Controls.Add(this.TextBoxPreco);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.TextBoxIdPacote);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtunSair);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormNovoPacote";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Adicionar Pacotes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtunSair;
        private System.Windows.Forms.Button ButtonNovoPacote;
        private System.Windows.Forms.TextBox TextBoxPreco;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.TextBox TextBoxIdPacote;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}