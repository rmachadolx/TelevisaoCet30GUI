﻿namespace ViagensLinq
{
    partial class FormEditarPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditarPacotes));
            this.TextBoxPreco = new System.Windows.Forms.TextBox();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.TextBoxIdPacote = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonFimIdPacote = new System.Windows.Forms.Button();
            this.ButtonAumentarIdPacote = new System.Windows.Forms.Button();
            this.ButtonDiminuirIdPacote = new System.Windows.Forms.Button();
            this.ButtonInicioIdPacote = new System.Windows.Forms.Button();
            this.ButtonSalvarPacote = new System.Windows.Forms.Button();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxPreco
            // 
            this.TextBoxPreco.Location = new System.Drawing.Point(232, 199);
            this.TextBoxPreco.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxPreco.Name = "TextBoxPreco";
            this.TextBoxPreco.Size = new System.Drawing.Size(232, 22);
            this.TextBoxPreco.TabIndex = 21;
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Location = new System.Drawing.Point(232, 101);
            this.TextBoxDescricao.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.Size = new System.Drawing.Size(638, 22);
            this.TextBoxDescricao.TabIndex = 20;
            // 
            // TextBoxIdPacote
            // 
            this.TextBoxIdPacote.Location = new System.Drawing.Point(232, 28);
            this.TextBoxIdPacote.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxIdPacote.Name = "TextBoxIdPacote";
            this.TextBoxIdPacote.ReadOnly = true;
            this.TextBoxIdPacote.Size = new System.Drawing.Size(232, 22);
            this.TextBoxIdPacote.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 199);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 16);
            this.label3.TabIndex = 18;
            this.label3.Text = "Preço ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 106);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 17;
            this.label2.Text = "Descrição :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "ID Pacote :";
            // 
            // ButtonFimIdPacote
            // 
            this.ButtonFimIdPacote.Image = ((System.Drawing.Image)(resources.GetObject("ButtonFimIdPacote.Image")));
            this.ButtonFimIdPacote.Location = new System.Drawing.Point(801, 15);
            this.ButtonFimIdPacote.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonFimIdPacote.Name = "ButtonFimIdPacote";
            this.ButtonFimIdPacote.Size = new System.Drawing.Size(68, 52);
            this.ButtonFimIdPacote.TabIndex = 25;
            this.ButtonFimIdPacote.UseVisualStyleBackColor = true;
            this.ButtonFimIdPacote.Click += new System.EventHandler(this.ButtonFimIdPacote_Click);
            // 
            // ButtonAumentarIdPacote
            // 
            this.ButtonAumentarIdPacote.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAumentarIdPacote.Image")));
            this.ButtonAumentarIdPacote.Location = new System.Drawing.Point(724, 15);
            this.ButtonAumentarIdPacote.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonAumentarIdPacote.Name = "ButtonAumentarIdPacote";
            this.ButtonAumentarIdPacote.Size = new System.Drawing.Size(68, 52);
            this.ButtonAumentarIdPacote.TabIndex = 24;
            this.ButtonAumentarIdPacote.UseVisualStyleBackColor = true;
            this.ButtonAumentarIdPacote.Click += new System.EventHandler(this.ButtonAumentarIdPacote_Click);
            // 
            // ButtonDiminuirIdPacote
            // 
            this.ButtonDiminuirIdPacote.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDiminuirIdPacote.Image")));
            this.ButtonDiminuirIdPacote.Location = new System.Drawing.Point(648, 15);
            this.ButtonDiminuirIdPacote.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonDiminuirIdPacote.Name = "ButtonDiminuirIdPacote";
            this.ButtonDiminuirIdPacote.Size = new System.Drawing.Size(68, 52);
            this.ButtonDiminuirIdPacote.TabIndex = 23;
            this.ButtonDiminuirIdPacote.UseVisualStyleBackColor = true;
            this.ButtonDiminuirIdPacote.Click += new System.EventHandler(this.ButtonDiminuirIdPacote_Click);
            // 
            // ButtonInicioIdPacote
            // 
            this.ButtonInicioIdPacote.Image = ((System.Drawing.Image)(resources.GetObject("ButtonInicioIdPacote.Image")));
            this.ButtonInicioIdPacote.Location = new System.Drawing.Point(572, 14);
            this.ButtonInicioIdPacote.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonInicioIdPacote.Name = "ButtonInicioIdPacote";
            this.ButtonInicioIdPacote.Size = new System.Drawing.Size(68, 52);
            this.ButtonInicioIdPacote.TabIndex = 22;
            this.ButtonInicioIdPacote.UseVisualStyleBackColor = true;
            this.ButtonInicioIdPacote.Click += new System.EventHandler(this.ButtonInicioIdPacote_Click);
            // 
            // ButtonSalvarPacote
            // 
            this.ButtonSalvarPacote.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtonSalvarPacote.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSalvarPacote.Image")));
            this.ButtonSalvarPacote.Location = new System.Drawing.Point(232, 290);
            this.ButtonSalvarPacote.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonSalvarPacote.Name = "ButtonSalvarPacote";
            this.ButtonSalvarPacote.Size = new System.Drawing.Size(117, 84);
            this.ButtonSalvarPacote.TabIndex = 14;
            this.ButtonSalvarPacote.UseVisualStyleBackColor = false;
            this.ButtonSalvarPacote.Click += new System.EventHandler(this.ButtonSalvarPacote_Click);
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ButtonSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonSair.Image = ((System.Drawing.Image)(resources.GetObject("ButtonSair.Image")));
            this.ButtonSair.Location = new System.Drawing.Point(765, 290);
            this.ButtonSair.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(104, 84);
            this.ButtonSair.TabIndex = 26;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // FormEditarPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 442);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.ButtonFimIdPacote);
            this.Controls.Add(this.ButtonAumentarIdPacote);
            this.Controls.Add(this.ButtonDiminuirIdPacote);
            this.Controls.Add(this.ButtonInicioIdPacote);
            this.Controls.Add(this.TextBoxPreco);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.TextBoxIdPacote);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonSalvarPacote);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormEditarPacotes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editar Pacotes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ButtonSalvarPacote;
        private System.Windows.Forms.TextBox TextBoxPreco;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.TextBox TextBoxIdPacote;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonInicioIdPacote;
        private System.Windows.Forms.Button ButtonDiminuirIdPacote;
        private System.Windows.Forms.Button ButtonAumentarIdPacote;
        private System.Windows.Forms.Button ButtonFimIdPacote;
        private System.Windows.Forms.Button ButtonSair;
    }
}