﻿using System;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
    public partial class Form1 : Form
    {
        private Tv minhaTV;
        public Form1()
        {
            InitializeComponent();
            minhaTV = new TelevisaoCet30GUI.Tv();
            LabelStatus.Text = minhaTV.Mensagem;
        }

      
        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            if (!minhaTV.GetEstado())
            {
                minhaTV.LigaTv();
                LabelStatus.Text = minhaTV.Mensagem;
                ButtonOnOff.Text = "Off";
                ButtonAumentaCanal.Enabled = true;
                ButtonDiminuiCanal.Enabled = true;
              
                LabelCanal.Text = minhaTV.Canal.ToString();
                LabelVolume.Text = minhaTV.Volume.ToString();
                TrackBarVolume.Enabled = true;
                TrackBarVolume.Value = minhaTV.Volume;
            }
            else
            {
                minhaTV.DesligaTv();
                LabelStatus.Text = minhaTV.Mensagem;
                ButtonOnOff.Text = "On";
                ButtonAumentaCanal.Enabled = false;
                ButtonDiminuiCanal.Enabled = false;
                
                LabelCanal.Text = "--";
                LabelVolume.Text = "--";
                TrackBarVolume.Enabled = false;
            }


        }

        private void ButtonAumentaCanal_Click(object sender, EventArgs e)
        {
            minhaTV.Canal++;
            LabelCanal.Text = minhaTV.Canal.ToString();
        }

     
     

        private void TrackBarVolume_Scroll(object sender, EventArgs e)
        {
            minhaTV.Volume=TrackBarVolume.Value;
            LabelVolume.Text = minhaTV.Volume.ToString();
        }

        private void LabelCanal_Click(object sender, EventArgs e)
        {

        }

        private void ButtonDiminuiCanal_Click(object sender, EventArgs e)
        {
            minhaTV.Canal--;
            LabelCanal.Text = minhaTV.Canal.ToString();
        }
    }
}
