﻿namespace TelevisaoCet30GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ButtonOnOff = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelCanal = new System.Windows.Forms.Label();
            this.ButtonAumentaCanal = new System.Windows.Forms.Button();
            this.ButtonDiminuiCanal = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TrackBarVolume = new System.Windows.Forms.TrackBar();
            this.LabelVolume = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(23, 273);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(114, 20);
            this.LabelStatus.TabIndex = 0;
            this.LabelStatus.Text = "Status da TV";
            // 
            // ButtonOnOff
            // 
            this.ButtonOnOff.Location = new System.Drawing.Point(90, 329);
            this.ButtonOnOff.Name = "ButtonOnOff";
            this.ButtonOnOff.Size = new System.Drawing.Size(75, 55);
            this.ButtonOnOff.TabIndex = 1;
            this.ButtonOnOff.Text = "On";
            this.ButtonOnOff.UseVisualStyleBackColor = true;
            this.ButtonOnOff.Click += new System.EventHandler(this.ButtonOnOff_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LabelCanal);
            this.groupBox1.Controls.Add(this.ButtonAumentaCanal);
            this.groupBox1.Controls.Add(this.ButtonDiminuiCanal);
            this.groupBox1.Location = new System.Drawing.Point(38, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 91);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Canais";
            // 
            // LabelCanal
            // 
            this.LabelCanal.AutoSize = true;
            this.LabelCanal.Location = new System.Drawing.Point(86, 47);
            this.LabelCanal.Name = "LabelCanal";
            this.LabelCanal.Size = new System.Drawing.Size(13, 13);
            this.LabelCanal.TabIndex = 2;
            this.LabelCanal.Text = "--";
            this.LabelCanal.Click += new System.EventHandler(this.LabelCanal_Click);
            // 
            // ButtonAumentaCanal
            // 
            this.ButtonAumentaCanal.Enabled = false;
            this.ButtonAumentaCanal.Location = new System.Drawing.Point(125, 36);
            this.ButtonAumentaCanal.Name = "ButtonAumentaCanal";
            this.ButtonAumentaCanal.Size = new System.Drawing.Size(44, 35);
            this.ButtonAumentaCanal.TabIndex = 1;
            this.ButtonAumentaCanal.Text = "+";
            this.ButtonAumentaCanal.UseVisualStyleBackColor = true;
            this.ButtonAumentaCanal.Click += new System.EventHandler(this.ButtonAumentaCanal_Click);
            // 
            // ButtonDiminuiCanal
            // 
            this.ButtonDiminuiCanal.Enabled = false;
            this.ButtonDiminuiCanal.Location = new System.Drawing.Point(15, 36);
            this.ButtonDiminuiCanal.Name = "ButtonDiminuiCanal";
            this.ButtonDiminuiCanal.Size = new System.Drawing.Size(42, 35);
            this.ButtonDiminuiCanal.TabIndex = 0;
            this.ButtonDiminuiCanal.Text = "-";
            this.ButtonDiminuiCanal.UseVisualStyleBackColor = true;
            this.ButtonDiminuiCanal.Click += new System.EventHandler(this.ButtonDiminuiCanal_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TrackBarVolume);
            this.groupBox2.Controls.Add(this.LabelVolume);
            this.groupBox2.Location = new System.Drawing.Point(38, 152);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(186, 118);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Volume";
            // 
            // TrackBarVolume
            // 
            this.TrackBarVolume.Enabled = false;
            this.TrackBarVolume.Location = new System.Drawing.Point(43, 48);
            this.TrackBarVolume.Maximum = 100;
            this.TrackBarVolume.Name = "TrackBarVolume";
            this.TrackBarVolume.Size = new System.Drawing.Size(104, 45);
            this.TrackBarVolume.TabIndex = 3;
            this.TrackBarVolume.Scroll += new System.EventHandler(this.TrackBarVolume_Scroll);
            // 
            // LabelVolume
            // 
            this.LabelVolume.AutoSize = true;
            this.LabelVolume.Location = new System.Drawing.Point(86, 16);
            this.LabelVolume.Name = "LabelVolume";
            this.LabelVolume.Size = new System.Drawing.Size(13, 13);
            this.LabelVolume.TabIndex = 2;
            this.LabelVolume.Text = "--";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 416);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ButtonOnOff);
            this.Controls.Add(this.LabelStatus);
            this.Name = "Form1";
            this.Text = "Televisão";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Button ButtonOnOff;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LabelCanal;
        private System.Windows.Forms.Button ButtonAumentaCanal;
        private System.Windows.Forms.Button ButtonDiminuiCanal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label LabelVolume;
        private System.Windows.Forms.TrackBar TrackBarVolume;
    }
}

