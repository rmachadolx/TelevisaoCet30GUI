﻿using System;
using System.IO;

namespace TelevisaoCet30GUI
{
    public class Tv
    {

        //atributos
        #region atributos

        private bool estado;

        private int canal;

        private string mensagem;

        private int volume;
    

        #endregion

        #region Propriedades

        public int Volume
        {
            get { return volume; }

            set { volume = value; }
        }


        public int Canal
        {
            get { return canal; }

            set
            {
                if (value >= 0 && value <= 20)
                {
                    canal = value;
                }
            } 
        }

        public string Mensagem
        {
            get;
            private set;
        }

        #endregion




        #region metodos
        //construtor
        public Tv()
        {
            estado = false;
            Canal = 1;
            Volume = 50;
            Mensagem = "Nova tv criada com sucesso";
        }

      

       
    
        public bool GetEstado()
        {
            return estado;
        }

        public void LigaTv()
        {
            if (!estado)
            {
                estado = true;
                LerInfo();
                Mensagem = "TV Ligada!";
            }
        }

        public void DesligaTv()
        {
            if (estado)
            {
                estado = false;
                GravarInfo();
                Mensagem = "TV Desligada";
            }
        }



        private void LerInfo()
        {
            string ficheiro = @"tvInfo.txt";

            StreamReader sr;
            if (File.Exists(ficheiro))
            {
                sr = File.OpenText(ficheiro);
                string linha = "";
                while ((linha = sr.ReadLine()) != null)
                {
                    string[] campos = new string[2];

                    campos = linha.Split(';');
                    Canal = Convert.ToInt32(campos[0]);
                    Volume = Convert.ToInt32(campos[1]);
                }
                sr.Close();

            }
        }
        private void GravarInfo()
        {
            string ficheiro = @"tvInfo.txt";
            string linha = Canal + ";" + Volume;
            StreamWriter sw = new StreamWriter(ficheiro, false);

            if (!File.Exists(ficheiro))
            {
                sw = File.CreateText(ficheiro);
            }
            sw.WriteLine(linha);
            sw.Close();
        }



        #endregion
    }
}
